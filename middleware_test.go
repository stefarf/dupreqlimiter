package dupreqlimiter

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"reflect"
	"sync"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
)

func TestMiddleware(t *testing.T) {
	const (
		hashExpired = time.Millisecond * 10
		threshold   = float32(0.5)
		numOfRun    = 50
		testBody    = "test body"
	)
	cleanUpInterval := time.Duration(float64(hashExpired) * 1.1)

	type endpointCode struct {
		endpoint string
		code     int
	}

	mapRecreated := false
	var latestEndpoints []string
	hashedAPILimiterMiddleware := Middleware(
		hashExpired,
		threshold,
		func(endpoints []string) {
			latestEndpoints = endpoints
			mapRecreated = true
		},
	)

	var result struct {
		sync.Mutex
		nextMap map[string]int
		codeMap map[endpointCode]int
	}
	result.nextMap = map[string]int{}
	result.codeMap = map[endpointCode]int{}
	next := func(c echo.Context) error {
		b, err := ioutil.ReadAll(c.Request().Body)
		ifTestErrFatal(t, err)
		if string(b) != testBody {
			t.Errorf("want body='%s', got '%s'", testBody, b)
		}

		result.Lock()
		defer result.Unlock()
		result.nextMap[c.Request().URL.Path]++
		return nil
	}

	runLimiter := func(endpoint string) {
		r := httptest.NewRequest("POST", endpoint, bytes.NewReader([]byte(testBody)))
		w := httptest.NewRecorder()
		e := echo.New()
		c := e.NewContext(r, w)
		f := hashedAPILimiterMiddleware(next)
		err := f(c)
		if err != nil {
			log.Fatal(err)
		}

		result.Lock()
		defer result.Unlock()
		result.codeMap[endpointCode{endpoint, w.Code}]++
	}

	var wg sync.WaitGroup
	for i := 0; i < numOfRun; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			runLimiter("/any/url")
		}()
	}
	for i := 0; i < numOfRun; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			runLimiter("/some/url")
		}()
	}
	wg.Wait()

	// Spec:
	// Reject request (do not run the next function and return 503)
	// if it has the same signature with the preceding request within hashExpired interval.
	// => Run the next function once for each signature
	assertDeepEqual(t, result.nextMap, map[string]int{
		"/any/url":  1,
		"/some/url": 1,
	}, "runNext")
	assertDeepEqual(t, result.codeMap, map[endpointCode]int{
		{"/any/url", http.StatusOK}:                  1,
		{"/any/url", http.StatusServiceUnavailable}:  numOfRun - 1,
		{"/some/url", http.StatusOK}:                 1,
		{"/some/url", http.StatusServiceUnavailable}: numOfRun - 1,
	}, "codeMap")

	time.Sleep(cleanUpInterval)
	if mapRecreated {
		t.Error("map should not be recreated yet")
	}
	runLimiter("/not/url")
	// Spec:
	// Reject request (do not run the next function and return 503)
	// if it has the same signature with the preceding request within hashExpired interval.
	// => Clear the internal table after:
	//    cleanUpInterval is passed
	//    and the actual clean up will take place when the runLimiter is executed
	if !mapRecreated {
		t.Error("expect map to be recreated")
	}
	if len(latestEndpoints) != 0 {
		t.Error("latestEndpoints should be zero size")
	}
}

func TestGetSignature(t *testing.T) {
	type testingItem struct {
		addr      string
		method    string
		endpoint  string
		body      string
		signature string
	}
	testingItems := []testingItem{
		{"10.0.0.1", "GET", "/url/one", "", "10.0.0.1||GET||/url/one||"},
		{"10.0.0.2", "GET", "/url/one", "", "10.0.0.2||GET||/url/one||"},
		{"10.0.0.2", "POST", "/url/one", "", "10.0.0.2||POST||/url/one||"},
		{"10.0.0.2", "POST", "/url/two", "", "10.0.0.2||POST||/url/two||"},
		{"10.0.0.2", "POST", "/url/two", "body", "10.0.0.2||POST||/url/two||body"},
		{"10.0.0.2", "POST", "/url/two", "body2", "10.0.0.2||POST||/url/two||body2"},
	}
	for idx, item := range testingItems {
		var r *http.Request
		if item.body == "" {
			r = httptest.NewRequest(item.method, item.endpoint, nil)
		} else {
			r = httptest.NewRequest(item.method, item.endpoint, bytes.NewReader([]byte(item.body)))
		}
		r.RemoteAddr = item.addr
		sig, err := getRequestSignature(r)
		ifTestErrFatal(t, err)
		if sig != item.signature {
			t.Errorf("[%d] want signature '%s', got '%s'", idx, item.signature, sig)
		}
	}
}

func assertDeepEqual(t *testing.T, got, want any, message string) {
	if !reflect.DeepEqual(got, want) {
		t.Errorf("%s: got %v, want %v\n", message, got, want)
	}
}

func ifTestErrFatal(t *testing.T, err error) {
	if err != nil {
		t.Fail()
	}
}
