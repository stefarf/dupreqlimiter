package dupreqlimiter

import (
	"bytes"
	"io"
	"io/ioutil"
)

type readCloserClone struct {
	r *bytes.Reader
}

func splitReadCloser(src io.ReadCloser) (io.ReadCloser, io.ReadCloser, error) {
	b, err := ioutil.ReadAll(src)
	if err != nil {
		return nil, nil, err
	}
	b1 := &readCloserClone{bytes.NewReader(b)}
	b2 := &readCloserClone{bytes.NewReader(b)}
	return b1, b2, nil
}

func (rc *readCloserClone) Read(p []byte) (n int, err error) {
	return rc.r.Read(p)
}

func (rc *readCloserClone) Close() error {
	return nil
}
