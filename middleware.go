package dupreqlimiter

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"github.com/labstack/echo/v4"

	"gitlab.com/stefarf/connratelimiter/autorecreatemap"
)

func Middleware(
	hashExpired time.Duration,
	threshold float32,
	recreated func(endpoints []string),
) echo.MiddlewareFunc {

	tbl := autorecreatemap.New[string, time.Time](threshold)
	if recreated != nil {
		tbl.Recreated = func(mActive map[string]time.Time) {
			var endpoints []string
			for endpoint := range mActive {
				endpoints = append(endpoints, endpoint)
			}
			recreated(endpoints)
		}
	}

	var mut sync.Mutex
	cleanUpInterval := time.Duration(float64(hashExpired) * 1.1)
	nextCleanUp := time.Now().Add(cleanUpInterval)

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			{
				mut.Lock()
				now := time.Now()
				if time.Now().After(nextCleanUp) {
					tbl.ForEach(func(hash string, t time.Time) (done bool) {
						if t.Add(hashExpired).Before(now) {
							tbl.Delete(hash)
						}
						return
					})
				}
				mut.Unlock()
			}

			{
				mut.Lock()
				now := time.Now()

				sig, err := getRequestSignature(c.Request())
				if err != nil {
					mut.Unlock()
					return c.String(http.StatusBadRequest, "Error: Can not read request body")
				}
				hash := calculateHash(sig)
				if t, ok := tbl.Get(hash); ok && t.Add(hashExpired).After(now) {
					mut.Unlock()
					return c.String(http.StatusServiceUnavailable, "Rejected: Duplicate API signature")
				}
				tbl.Set(hash, now)
				mut.Unlock()
			}

			return next(c)
		}

	}
}

func calculateHash(s string) string {
	sum := sha1.Sum([]byte(s))
	return hex.EncodeToString(sum[:])
}

func getRequestSignature(r *http.Request) (string, error) {
	ipAddr := getRemoteAddress(r)
	method := r.Method
	endpoint := r.URL.Path

	newBody, bodyClone, err := splitReadCloser(r.Body)
	if err != nil {
		return "", err
	}
	r.Body = newBody

	body, err := readRequestBody(bodyClone)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s||%s||%s||%s", ipAddr, method, endpoint, body), nil
}

func readRequestBody(body io.ReadCloser) (string, error) {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func getRemoteAddress(r *http.Request) (addr string) {
	addr = r.Header.Get("Cf-Connecting-Ip")
	if addr != "" {
		return
	}
	addr = r.Header.Get("X-Forwarded-For")
	if addr != "" {
		return
	}
	addr = r.Header.Get("X-Real-Ip")
	if addr != "" {
		return
	}
	return r.RemoteAddr
}
